package nl.bioinf.junit.junit5demos;

import java.util.Arrays;

public enum Continent {
    EUROPE("Europe", "Eur", 658),
    ASIA("Asia", "As", 3598);

    private final String name;
    private final String abbreviation;
    private final int population;

    Continent(String name, String abbreviation, int population) {
        this.name = name;
        this.abbreviation = abbreviation;
        this.population = population;
    }

    @Override
    public String toString() {
        return "Continent{" +
                "name='" + name + '\'' +
                ", abbreviation='" + abbreviation + '\'' +
                ", population=" + population +
                '}';
    }

    public static void main(String[] args) {
        Continent C1 = Continent.ASIA;
        System.out.println(C1);

        Continent C2 = Continent.valueOf("EUROPE");
        System.out.println(C2);


    }
}
