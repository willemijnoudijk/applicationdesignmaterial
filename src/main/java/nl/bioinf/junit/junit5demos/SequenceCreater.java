package nl.bioinf.junit.junit5demos;

import java.util.Set;

public class SequenceCreater {
    // test kan er nu niet bij door private
    private static Set<Character> dnaNucleotides;

    /**
     * Creates random sequence of the given length and type.
     * Length must be equal or above zero
     *
     * @param length
     * @param type
     * @return randomSequence the random sequence
     */

    public static String createRandomSequence(int length, String type) {

        if (length < 0) {
            throw new IllegalArgumentException("length must be at least zero"); //unchecked exception > hoeft niet in de method gedeclareerd te worden
        }
        String randomSequence = "";
        return randomSequence;

        // mag weg > gebruiken als je methode nog niet geimplementeerd hebt
        //throw new UnsupportedOperationException("not implemented yet");
    }

    // when you want to test a private method
    /* private -> testing */ void privateDemo() {}
}
