package nl.bioinf.designpatterns.observer_filter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * The real time sequencer implementation; a dummy fr now.
 * Created by michiel on 07/03/2017.
 */
public class Sequencer {
    List<SequencerObserver> seqObservers = new LinkedList<>();
    List<Sequence> dummySeqs;

    public void registerObserver(SequencerObserver seqObserver) {
        this.seqObservers.add(seqObserver);
    }

    /**
     * unsubscribe.
     *
     * @param seqObserver
     * @return
     */
    public boolean unregisterObserver(SequencerObserver seqObserver) {
        if (this.seqObservers.contains(seqObserver)) {
            this.seqObservers.remove(seqObserver);
            return true;
        }
        return false;
    }

    /**
     * doing the actual sequencing work.
     */
    public void sequence() {
        //going to generate sequences here
        for (Sequence seq : this.dummySeqs) {
            System.out.println(seq);
            for (SequencerObserver obs : this.seqObservers) {
                //System.out.println("obs.getClass().getSimpleName() = " + obs.getClass().getSimpleName());
                obs.newSequenceDetermined(seq);
            }
        }
    }

    /**
     * obvious.
     *
     * @param number
     * @param length
     */
    public void createDummySequences(int number, int length) {
        if (null == dummySeqs) {
            dummySeqs = new ArrayList<>();
        }
        final char[] nucs = {'A', 'G', 'C', 'T'};
        final char[] qual = {'!', '$', '0', '1', '5', '9', 'A', 'C', 'E', 'I'};

        for (int i = 0; i < number; i++) {
            StringBuilder sbNucs = new StringBuilder();
            StringBuilder qualNucs = new StringBuilder();
            for (int j = 0; j < length; j++) {
                char n = nucs[(int) (Math.random() * nucs.length)];
                sbNucs.append(n);
                char q = qual[(int) (Math.random() * qual.length)];
                qualNucs.append(q);
            }
            Sequence s = new Sequence(sbNucs.toString(), qualNucs.toString(), (long) i);
            dummySeqs.add(s);
        }
    }

}
