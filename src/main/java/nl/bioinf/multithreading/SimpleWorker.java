package nl.bioinf.multithreading;

public class SimpleWorker implements Runnable {
    private String name;
    private static int operationCount;

    public SimpleWorker(String name) {
        this.name = name;
    }

    public static void incrementOperationCount() {
        operationCount++;
    }

    @Override
    public void run() {
        synchronized (SimpleWorker.class) {
            incrementOperationCount();
            System.out.println("S Worker " + name + " doing my thing");
            System.out.println("Operation count =  " + operationCount);
        }
    }
}
