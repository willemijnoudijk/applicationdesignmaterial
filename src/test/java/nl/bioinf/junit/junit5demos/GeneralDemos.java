package nl.bioinf.junit.junit5demos;


import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class GeneralDemos {

    @Test
    public void testDoubleEquals() {
        double one = 0.234566;
        double two = 0.234567;

        assertEquals(one, two, 0.00001);
    }

    @Test
    public void testImportant() {
        String first = "Michiel";
        String second = "Michiel";
        assertSame("these should be the same object..?", first, second);
    }

    /**
     * parameterized test with single value
     */
    @DisplayName("Palindrome finding")
    @ParameterizedTest(name = "\"{0}\" should be a palindrome")
    @ValueSource(strings = {"racecar", "radar", "able was I ere I saw elba"})
    void palindromes(String candidate) {
        assertTrue(isPalindrome(candidate), () -> candidate + " should be a palindrome!");
    }

    private boolean isPalindrome(String candidate) {
        return false;
    }

    /**
     * parameterized test with two values
     */
    @DisplayName("Roman numeral")
    @ParameterizedTest(name = "\"{0}\" should be {1}")
    @CsvSource({"I, 1", "II, 2", "V, 5"})
    void withNiceName(String word, int number) {
    }

}
