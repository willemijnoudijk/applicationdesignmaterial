package nl.bioinf.junit.junit5demos;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SequenceCreaterTest {

    @BeforeEach
    void setUp(){
        System.out.println("initializing before each test");
}

    @BeforeAll
    static void setUpClass(){
        System.out.println("initializing before class");
    }

    @Test
    void createRandomSequenceSunny() {
        // TODO have to create test
        // view - tool windows - Toodo: alles kijken
        // Wanneer je niet meteen aan de test werkt:>
        //fail("not implemented yet");

        //sunny day scenario: de goede manier
        SequenceCreater.createRandomSequence(10, "DNA");
        SequenceCreater.createRandomSequence(10, "RNA");
        //SequenceCreater.createRandomSequence(10, "proteim");


        // moet ook testen op legale input die niet klopt

    }
    @Test
    @DisplayName("This tests a negative length sequence argument")
    void createRandomSequenceNegativeLength() {

        try {
            SequenceCreater.createRandomSequence(-1, "DNA");
            fail("Should have thrown an exception");
        } catch (IllegalArgumentException ex){
            // nothing to do > de methode wil de exceptie juist hebben
        }
    }

    @Test
    void createRandomSequenceZeroLength() {
        // dit is boundary case
        String sequence = SequenceCreater.createRandomSequence(0, "DNA");
        assertEquals("", sequence, "should have returned an empty string");
    }

    @Test
    void createRandomSequenceWrongType() {
        SequenceCreater.createRandomSequence(10, "rope");
        fail("not implemented yet"); //or assert..
    }
}